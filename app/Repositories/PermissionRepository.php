<?php namespace App\Repositories;


use App\Permission;

class PermissionRepository extends Repository
{

    public function model()
    {
       return Permission::class;
    }
}
