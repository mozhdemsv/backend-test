<?php namespace App\Repositories;

abstract class Repository
{
    protected $model;

    public function __construct()
    {
        $this->model = app($this->model());
    }

    abstract public function model();


    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function findWith($id,$rel_model){
        return $this->model->with($rel_model)->find($id);
    }

    public function update($model, array $data)
    {
        return $model->update($data);
    }

}
