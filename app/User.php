<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','api_token'
    ];

    public function permissions(){
       return $this->belongsToMany(Permission::class);
    }

    public function roles(){
      return  $this->belongsToMany(Role::class);
    }

    public function hasRole($roles){

        return !! $roles->intersect($this->roles)->all();
    }

    public function hasPermission($permission){

        return ($this->permissions->contains('name',$permission->name) || $this->hasRole($permission->roles));
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','api_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
