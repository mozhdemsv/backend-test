<?php

namespace App\Http\Requests\Api\v1;

use Illuminate\Foundation\Http\FormRequest;

class RegisterFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ];
    }

//    public function messages()
//    {
//        return [
//            'name.required'=>'فیلد نام الزامی است.',
//            'email.required'=>'فیلد ایمیل الزامی است',
//            'email.exists'=>'کاربر معتبر نمیباشد',
//            'password.required'=>'فیلد پسورد الزامی است'
//
//        ];
//    }
}
