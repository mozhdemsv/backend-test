<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\v1\Admin\UpdateUserFormRequest;
use App\Http\Requests\Api\v1\LoginFormRequest;
use App\Http\Requests\Api\v1\RegisterFormRequest;
use App\Http\Resources\v1\Answer;
use App\Http\Resources\v1\User as UserResource;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;

class UserController extends Controller
{
    private $user;

    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    public function edit($id)
    {
        $user=$this->user->findWith($id,'roles');
        if($user)
            return new Answer($user);
        return new Answer(['کاربری یافت نشد.']);

    }

    public function update(UpdateUserFormRequest $request){

        $user=$this->user->find($request->id);
       if ($user){
           $user->roles()->sync($request['roles']);
           return new Answer(['نقش کاربری با موفقیت ویرایش شد.']);
       }
        return new Answer(['کاربری یافت نشد.']);
    }
    public function login(LoginFormRequest $request){

        if (! auth()->attempt([
            'email'=>$request->email,
            'password'=> $request->password
        ]))
            return new Answer(['اطلاعات شما صحیح نیست.']);
        return new UserResource(auth()->user());
    }

    public function register(RegisterFormRequest $request){

       $user=User::create([
           'name'=>$request->name,
           'email'=>$request->email,
           'password'=>bcrypt($request->password),
           'api_token'=>Str::random(100),
       ]);

       return new UserResource($user);

    }
}
