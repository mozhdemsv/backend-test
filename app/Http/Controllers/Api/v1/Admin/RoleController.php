<?php

namespace App\Http\Controllers\Api\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\v1\Admin\RoleFormRequest;
use App\Http\Resources\v1\Answer;
use App\Repositories\RoleRepository;

class RoleController extends Controller
{
    private $roles;

    public function __construct(RoleRepository $roles)
    {
        $this->roles = $roles;
    }

    public function store(RoleFormRequest $request)
    {
        $role=$this->roles->create($request->all());
        $role->permissions()->sync($request['permissions']);
        return new Answer(['نقش کاربری با موفقیت ایجاد شد.']);
    }


}
