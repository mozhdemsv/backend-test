<?php

namespace App\Http\Controllers\Api\v1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\v1\Admin\PermissionFormRequest;
use App\Http\Resources\v1\Answer;
use App\Repositories\PermissionRepository;
use Illuminate\Http\Request;

class PermissinController extends Controller
{
    private $permissions;

    public function __construct(PermissionRepository $permissions)
    {
        $this->permissions = $permissions;
    }

    public function store(PermissionFormRequest $request)
    {
        $this->permissions->create($request->all());
        return new Answer(['سطح دسترسی با موفقیت ثبت شد.']);
    }


}
