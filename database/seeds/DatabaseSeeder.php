<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $user=factory(\App\User::class)->create(['name'=>'admin']);
        $permission=factory(\App\Permission::class)->create(['name'=>'admin']);
        $role=factory(\App\Role::class)->create(['name'=>'admin']);
        $user->permissions()->sync($permission->id);
        $user->roles()->sync($role->id);
    }
}
