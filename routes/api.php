<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('v1')->namespace('Api\v1')->group(function(){
    Route::post('login','UserController@login');
    Route::post('register','UserController@register');
    Route::group(['middleware' => ['auth:api', 'can:admin']],function (){
        Route::get('user/edit/{user}','UserController@edit');
        Route::post('user/update','UserController@update');
    });

});
Route::prefix('v1')->namespace('Api\v1\Admin')->group(function(){
    Route::group(['middleware' => ['auth:api', 'can:admin']],function (){
        Route::post('permission/store','PermissinController@store');
        Route::post('role/store','RoleController@store');
    });
});



//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
